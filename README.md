Wake up. Shower. Grab a coffee. Walk to Class. Enjoy the views. Call up friends and play some pool. Hit the books in the study room. Blow off some steam and hit the weights. Ditch your old place. The newest student community in Charleston is ready for you.




Address: 28 Woolfe St, Charleston, SC 29403 || Phone: 843-321-4638